# Funcionamiento general

logReader realiza la lectura línea por línea de un archivo de log, enviando cada una mediante
conexiones de sockets TCP, una por cada ID de equipo encontrado.

> El script python `server.py` funciona como servidor de escucha de las líneas enviadas por el
*SocketTCP*, el mismo escucha en *localhost* puerto *5555*. Esto solo se realizó a modo de prueba del envío de las líneas del log.

# Realizar la prueba en local
Se ejecuta el script python, ubicandose en el directorio en el cual se ubica y ejecutando:
```py
python3 server.py
```
Luego de ejecutamos el logReader (leer siguiente sección).

# Llamada a `logReader`
Ejecutamos el método `main` de `Main`, el cual está configurado para leer el archivo
cuyo nombre pasamos siempre como **primer parámetro** y cuya ubicación es `home/jcontroller/logs`.

Los siguientes parámetros obligatorios son el horario de inicio y el de fin, respectivamente.
Así una llamada simple de ejemplo al script resulta:
```bash
java -jar logReader.jar -n Trafic_2022-11-03.log -s 09:00:00 -e 09:04:59
```

De forma genérica:
```java
java -jar logReader.jar -n <nombre_archivo> -s <hora_inicio> -e <hora_fin>
```

Un parámetro adicional que podemos dar es el puerto del localhost al que se enviará cada línea,
este parámetro se pasa como cuarto argumento, luego del horario de fin:
```bash
java -jar logReader.jar -n Trafic_2022-11-03.log -s 09:00:00 -e 09:04:59 -p 16001
```

De forma genérica:
```java
java -jar logReader.jar -n <nombre_archivo> -s <hora_inicio> -e <hora_fin> -p <puerto>
```

Otro parámetro opcional da la posibilidad de filtrar las tramas dando un regex como argumento en la llamada:

```bash
java -jar logReader.jar -n Trafic_2022-11-03.log -s 09:00:00 -e 09:04:59 -p 16001 -f \>RUS
```

De forma genérica:
```java
java -jar logReader.jar -n <nombre_archivo> -s <hora_inicio> -e <hora_fin> -p <puerto> -f <regex>
```

Al ejecutarlo simplemente envía linea por línea del archivo indicado.

En cada línea se lee el ID del equipo y el caracter que indica el tipo de trama (I=Input, O=Output,D=Disconection).

# Descripción
Se cuenta con las siguientes clases:
* `TCPConnection`: Clase para crear la conexion de socket TCP
* `LogReader`: Clase para realizar la lectura de un archivo log y enviar las lineas por el socket utilizando la clase `TCPConnection`
* `Main`: Clase principal para ejecutar el programa, se llama con los parámetros:
    * *ruta al archivo de log*
    * *ip del servidor*
    * *puerto del servidor*

# Lectura de log, manejo de conexiones y envío de datos
* Se crea un HashMap donde se guarda la conexión del socket TCP con el ID del equipo correspondiente:
```java
Map<String, TCPConnection> TCPConnectionMap = new HashMap<>();
```

* Se lee línea por línea del log mediante el método `readContent` de la clase `logReader`. Mediante *regex* se obtienen el ID del equipo y el caracter que indica si se conectó o desconectó:
    * Si el caracter es `D` se busca en el HashMap la conexión del equipo, se cierra y luego se elimina del mismo:
    ```java
    TCPConnectionMap.get(id).closeConnection();
    TCPConnectionMap.remove(id);
    ```
    * Si no es `D` se verifica si existe una conexión en el HashMap correspondiente al ID del equipo:
        * Si ya existe se envía la línea por el socket correspondiente.
        * Si no existe se crea una conexión en el HashMap asignada al ID del equipo y luego se realiza el envío.
    ```java
    if (!TCPConnectionMap.containsKey(id)) {
        System.out.println("NUEVA CONEXION EQUIPO: " + id);
        TCPConnectionMap.put(id, new TCPConnection(host, port));
    }
    TCPConnectionMap.get(id).sendData((text));
    ```