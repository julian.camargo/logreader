from socket import *
from time import ctime
import threading
 
HOST = '127.0.0.1'
PORT = 5555
BUFSIZ = 1024
ADDR = (HOST, PORT)
 
tcpSerSock = socket(AF_INET, SOCK_STREAM)
tcpSerSock.bind(ADDR)
tcpSerSock.listen(5)
socks = [] # poner el enchufe de cada cliente
 
print('tcp server started. IP is %s, port: %d' % (HOST, PORT)) 
 
def clientthread_handle():
    while True:
        for s in socks:
            try:
                data = s.recv(BUFSIZ) # Aquí el programa continúa ejecutándose hacia abajo
                # s.send("200 OK".encode())
                if len(data)==0:
                    print ("-- El otro extremo está desconectado")
                    s.close()
                    socks.remove(s)
                    continue
                else:
                    print("DATOS LEIDOS: ", data)
                    str2send = ('[%s],%s' % (ctime(), data))
                    #s.send(str2send.encode())
                    s.send(bytes("200 OK\n", 'utf-8'))
                    print("RESPUESTA ENVIADA.")
            except Exception as e:
                continue
 
t = threading.Thread (target = clientthread_handle) # hilo secundario
if __name__ == '__main__':
    t.start()
    print( 'waiting for connecting...')
    while True:
        clientSock, addr = tcpSerSock.accept()
        print(  '-- Connected from:', addr)
        clientSock.setblocking(0)
        socks.append(clientSock)
