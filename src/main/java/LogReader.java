import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase para realizar la lectura de un archivo log
 */
public class LogReader {

    private static final Logger logger = Logger.getLogger(LogReader.class.getName());

    public LogReader() {
    }

    /**
     * Método para obtener la trama pura de la línea leída
     *
     * @return String - trama pura sin cabeceras de ID, hora, canal, etc.
     */
    private static String obtainFrame(String readedLine, String regex) {
        String frame = null;
        Pattern framePattern = Pattern.compile("\\#]\\s.*");
        Matcher frameMatcher = framePattern.matcher(readedLine);

        if (frameMatcher.find()) {
            frame = frameMatcher.group(0).substring(3);
        } else {
            logger.warning("Frame not found.");
            frame = null;
        }
        return frame;
    }

    /**
     * Método para filtrar tramas por una regex dada en la llamada al script
     * Si no se especifica una regex (String regex null o vacío) el método devuelve true.
     *
     * @param frame String - trama de entrada a filtrar
     * @param regex String - filtro regex
     * @return boolean - true en caso de que la trama pase el filtro, false en caso contrario
     */
    private static boolean selectedFrame(String frame, String regex) {
        if (regex != null && regex != "") {
            Pattern selectedFramePattern = Pattern.compile(regex);
            Matcher selectedFrameMatcher = selectedFramePattern.matcher(frame);
            return selectedFrameMatcher.find();
        } else {
            return true;
        }
    }

    /**
     * Lectura del archivo de text (log)
     *
     * @param filePath  ruta al archivo a leer
     * @param host      dirección ip del controller al que se enviará cada línea del log
     * @param port      puerto del controller al que se enviará cada línea del log
     * @param startHour
     * @param endHour
     * @throws IOException
     */
    public void readContent(String filePath, String host, int port, String startHour, String endHour, String regex) throws IOException, ParseException {
        ArrayList framesSended = new ArrayList();
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        BufferedReader nextLineReader = new BufferedReader(new FileReader(filePath));

        AtomicReference<Integer> processedFrames = new AtomicReference<>(0);
        AtomicReference<Integer> errorFrames = new AtomicReference<>(0);

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        Date startHourDate = sdf.parse(startHour);
        Date endHourDate = sdf.parse(endHour);

        Pattern patternlineHeader = Pattern.compile("[0-9][0-9]\\:[0-9][0-9]\\:[0-9][0-9] \\[[A-Z]{3,7}\\]");
        Pattern patternId = Pattern.compile("\\.*ID:[0-9]{6}\\.*");
        Pattern patternHourId = Pattern.compile("[0-9][0-9]:[0-9][0-9]:[0-9][0-9]");
        Pattern patternFrameType = Pattern.compile("\\.*\\s[I|O|D|C|U]\\.*");

        Matcher matcherLine = null;
        Matcher matcherId;
        Matcher matcherHour;
        Matcher matcherFrameType;


        String text = reader.readLine();

        nextLineReader.readLine();
        String nextLine = null;

        Map TCPConnectionMap = new HashMap();
        List<Thread> threadList = new LinkedList<>();

        String id;
        String frameType;
        Date frameHour = null;
        String stringFrameHour = "";
        String frame;

        while (text != null) {
            nextLine = nextLineReader.readLine();
            if (nextLine != null) {
                matcherLine = patternlineHeader.matcher(nextLine);
                while (!matcherLine.find() && !nextLine.isEmpty()) {
                    text += nextLine;
                    nextLine = nextLineReader.readLine();
                    matcherLine = patternlineHeader.matcher(nextLine);
                    reader.readLine();
                }
            }
            matcherFrameType = patternFrameType.matcher(text);
            if (matcherFrameType.find()) {
                frameType = matcherFrameType.group(0).trim();
            } else {
                logger.warning("Connection parameter not found: ");
                frameType = "";
            }
            /**
             * Si la trama no es tipo INPUT ni DISCONNECT pasa a la siguiente.
             */
            if (!frameType.equals("I") && !frameType.equals("D")) {
                text = reader.readLine();
                continue;
            }

            matcherId = patternId.matcher(text);
            if (matcherId.find()) {
                id = matcherId.group(0).substring(3);
            } else {
                logger.warning("ID not found");
                text = reader.readLine();
                continue;
            }

            matcherHour = patternHourId.matcher(text);
            if (matcherHour.find()) {
                stringFrameHour = matcherHour.group(0);
                frameHour = sdf.parse(stringFrameHour);
            } else {
                logger.warning("Hour parameter not found");
            }

            boolean matchFrameHour = (frameHour.getTime() >= startHourDate.getTime() && frameHour.getTime() <= endHourDate.getTime());
            if (matchFrameHour) {
                logger.info("HOUR:" + stringFrameHour);
                boolean selectedFrameBool = (regex.isEmpty() || regex == null ? true : selectedFrame(text, regex));
                if (!selectedFrameBool) {
                    text = reader.readLine();
                    continue;
                }

                frame = obtainFrame(text, regex); //--note acá obtiene la trama pura.

                if (frameType.equals("D") && TCPConnectionMap.containsKey(id)) {
                    ((TCPConnection) TCPConnectionMap.get(id)).closeConnection();
                    logger.warning("TCP Socket connection closed.");
                    TCPConnectionMap.remove(id);
                    logger.info("TCP Socket connection deleted");
                } else if (frame != null && !frame.isEmpty()) {
                    if (!TCPConnectionMap.containsKey(id)) {
                        logger.info("Opening new TCP Socket connection.");
                        // todo -- Verificar que exista la conexion al socket, sino crearla
                        TCPConnectionMap.put(id, new TCPConnection(host, port));
                        logger.info("TCP Socket connection created");
                    }
                    String finalFrame = frame;
                    String finalId = id;
                    String finalText = text;
                    String finalId1 = id;
                    Thread sendMessage = new Thread(() -> {
                        try {
                            logger.info("Sending log.");
                            ((TCPConnection) TCPConnectionMap.get(finalId)).sendData(finalFrame);
                            logger.info(finalFrame);
                            // note  --Acá se traba esperando respuesta en el jcontroller
                            String response = ((TCPConnection) TCPConnectionMap.get(finalId)).readResponse();
                            if (response.contains("SAK")) processedFrames.getAndSet(processedFrames.get() + 1);
                            else errorFrames.getAndSet(errorFrames.get() + 1);
                            System.out.println("RESPUESTA: " + response);
                            framesSended.add(finalText);
                            logger.info("Log sent succesfully.");
                        } catch (IOException e) {
                            logger.severe("Socket cerrado inesperadamente para equipo: " + finalId1);
                        }
                    });
                    sendMessage.start();
                    threadList.add(sendMessage);
                }
            }
            if (frameHour.getTime() > endHourDate.getTime()) {
                break;
            } else {
                text = reader.readLine();
            }
        }
        try {
            for (Thread thread : threadList) {
                thread.join();
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        // -- TODO Resumen de las tramas enviadas
        logger.info("Tramas enviadas: ");
        for (int i = 0; i < framesSended.size(); i++) {
            logger.info((String) framesSended.get(i));
        }
        logger.info("\u001B[32m\n" + "Tramas procesadas: " + processedFrames +
                "\nTramas erróneas: " + errorFrames +
                "\nTotal tramas enviadas: " + (processedFrames.get() + errorFrames.get()));

    }
}
