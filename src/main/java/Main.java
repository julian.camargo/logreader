import java.io.IOException;
import java.text.ParseException;

public class Main {
//    static final String HOST = "34.121.2.169";
    static final String HOST = "35.193.199.42";
    static final int DEFAULT_PORT = 5555;

    public static void main(String[] args) throws IOException, ParseException {
        LogReader logReader = new LogReader();
        String name = null;
        int port = DEFAULT_PORT;
        String startHour = null;
        String endHour = null;
        String regex = "";

        // TODO: deharcodear esta parte
        for (int i=0; i < args.length; i++) {
            if(args[i].equals("-n")) {
                name = args[i+1];
            }
            if(args[i].equals("-p")) {
                port = Integer.parseInt(args[i + 1]);
            }
            if(args[i].equals("-s")) {
                startHour = args[i+1];
            }
            if(args[i].equals("-e")) {
                endHour = args[i+1];
            }
            if(args[i].equals("-f")) {
                regex = args[i+1];
            }
        }

        System.out.println("Path: " + "/home/jcontroller/logs/" + name);
        System.out.println("Port: " + port);
        System.out.println("Start hour: " + startHour + " --- " + "End hour: " + endHour);
        System.out.println("Filter: " + regex + "\n");

        logReader.readContent("/home/sitrack/Descargas/" + name, HOST, port, startHour, endHour, regex);
    }
}