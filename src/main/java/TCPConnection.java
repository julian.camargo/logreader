import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

/**
 * Clase para crear la conexion de socket TCP
 */
public class TCPConnection {
    public String host;
    public int port;
    public Socket socketConnection;
    public DataOutputStream out;
    public DataInputStream in;

    /**
     * Constructor donde se crea el socket TCP
     *
     * @param host dirección IP
     * @param port puerto
     */
    public TCPConnection(String host, int port) {
        this.host = host;
        this.port = port;

        try {
            socketConnection = new Socket(host, port);
            out = new DataOutputStream(socketConnection.getOutputStream());
            in = new DataInputStream(socketConnection.getInputStream());
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Método para enviar datos mediante un socket TCP
     *
     * @param dataContent contenido a enviar
     * @throws IOException
     */
    public void sendData(String dataContent) throws IOException {
        if (!dataContent.isEmpty()) out.write(dataContent.getBytes());
        out.flush();
    }

    /**
     * Lectura de la respuesta del servidor a la trama enviada
     *
     * @return String - respuesta servidor
     * @throws IOException
     */
    public String readResponse() {
        byte[] messageBytes = new byte[1024];
        String response = "";
        int tries = 0;
        try {
            while (tries < 100 && response.isEmpty()) {
                if (in.available() > 0) {
                    in.read(messageBytes, 0, messageBytes.length);
                    response = new String(messageBytes, StandardCharsets.UTF_8).trim();
                } else {
                    TimeUnit.MILLISECONDS.sleep(20);
                }
                tries++;
            }
        } catch (IOException | InterruptedException exception) {
            System.out.println(exception.getMessage());
        }
        return response;
    }

    /**
     * Método para cerrar la conexión creada en el contructor de la clase
     *
     * @throws IOException
     */
    public void closeConnection() throws IOException {
        socketConnection.close();
    }
}
